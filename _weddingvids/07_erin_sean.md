---
title: Erin & Sean - Short Film Wedding Trailer by Color Room Films
subtitle: Your Backyard Farm Wedding
location:
link: 190136625
thumb: /img/thumbs/erin_sean.jpg
tags: find-your-style
wedding-page: 231079840
---