---
title: Brittany & Joe - Short Film Wedding Trailer by Color Room Films
subtitle: Your Winter Ballroom Wedding
location:
link: 199039014
thumb: /img/thumbs/brittany_joe.jpg
tags: find-your-style
wedding-page: 236801137
---