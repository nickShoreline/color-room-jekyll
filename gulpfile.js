// Require Gulp
var gulp = require('gulp');
var prefix = require('gulp-autoprefixer');
var watch = require('gulp-watch');
var critical = require('critical');
var exec = require('child_process').exec;

var jekyll   = process.platform === 'win32' ? 'jekyll.bat' : 'jekyll';
var messages = {
    build: 'Building jekyll site...',
    serve: 'Serving jekyll site...'
};

var commands = {
  clean: 'bundle exec jekyll clean',
  polish: 'gulp autoprefix && gulp critical',
  build: 'JEKYLL_ENV=development bundle exec jekyll build -c _config.yml,_config_dev.yml',
  serve: 'JEKYLL_ENV=development bundle exec jekyll serve -c _config.yml,_config_dev.yml --host 0.0.0.0 --force_polling --incremental',
  build_prod: 'JEKYLL_ENV=production bundle exec jekyll build'
};

/**
 * Build the Jekyll Site
 */
gulp.task('build', function (cb) {
  console.log(messages.build);
  return exec(commands.clean + ' && ' + commands.build + ' && ' + commands.polish + ' && ' + commands.build, function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});
gulp.task('build_prod', function (cb) {
  console.log(messages.build);
  return exec(commands.clean + ' && ' + commands.build_prod + ' && ' + commands.polish + ' && ' + commands.build_prod, function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    console.log('fired');
    cb(err);
  });
});

/**
 * Serve the Jekyll Site
 */
gulp.task('serve', function (cb) {
  console.log(messages.build);
  return exec(commands.clean + ' && ' + commands.serve, function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});


// Gulp Sass Task
gulp.task('autoprefix', function() {
  gulp.src('_site/assets/main.css')
    .pipe(prefix({
			browsers: ['> 1%', 'last 4 versions', 'ie > 10']
    }))
    .pipe(gulp.dest('_site/assets'));
});

gulp.task('critical', ['autoprefix'], function () {
  critical.generate({
    base: './',
    src: '_site/index.html',
    css: '_site/assets/main.css',
    dest: '_includes/critical.css',
    width: 414,
    height: 736,
    minify: true,
    // Extract inlined styles from referenced stylesheets
    extract: true
  });
});

gulp.task('watch', function() {
  gulp.watch(['_site/assets/**/*.css'], ['autoprefix']);
});

gulp.task('default', ['serve', 'watch']);
