var weddingVid = [
  {
    "title": "Kristin & Michael-Wedding Trailer",
    "link": "236026743",
    "thumb": "/img/thumbs/kristen_michael.jpg"
  },
  {
    "title": "Lingie & Ned-Wedding Trailer",
    "link": "223532698",
    "thumb": "/img/thumbs/lingie_ned.jpg"
  },
  {
    "title": "Danielle & Tyler-Wedding Trailer",
    "link": "237608486",
    "thumb": "/img/thumbs/danielle_tyler.jpg"
  },
  {
    "title": "Gina & Billy -Wedding Trailer",
    "link": "231079840",
    "thumb": "/img/thumbs/gina_billy.jpg"
  },
  {
    "title": "Jillian & Matthew-Wedding Trailer",
    "link": "232832728",
    "thumb": "/img/thumbs/jillian_matthew.jpg"
  },
  {
    "title": "Jessica & Benji-Wedding Trailer",
    "link": "233336221",
    "thumb": "/img/thumbs/jessica_benji.jpg"
  }, {
    "title": "Ashley & Justin-Wedding Trailer",
    "link": "233693465",
    "thumb": "/img/thumbs/ashley_justin.jpg"
  },
  {
    "title": "Darya & Lee -Wedding Trailer",
    "link": "236801137",
    "thumb": "/img/thumbs/darya_lee.jpg"
  },
  {
    "title": "Stefanie & Frank-Wedding Trailer",
    "link": "199419146",
    "thumb": "/img/thumbs/stefanie_frank.jpg"
  }



];





!function(t){"use strict";t.fn.fitVids=function(e){var i={customSelector:null,ignore:null};if(!document.getElementById("fit-vids-style")){var r=document.head||document.getElementsByTagName("head")[0],a=document.createElement("div");a.innerHTML='<p>x</p><style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>',r.appendChild(a.childNodes[1])}return e&&t.extend(i,e),this.each(function(){var e=['iframe[src*="player.vimeo.com"]','iframe[src*="youtube.com"]','iframe[src*="youtube-nocookie.com"]','iframe[src*="kickstarter.com"][src*="video.html"]',"object","embed"];i.customSelector&&e.push(i.customSelector);var r=".fitvidsignore";i.ignore&&(r=r+", "+i.ignore);var a=t(this).find(e.join(","));(a=(a=a.not("object object")).not(r)).each(function(){var e=t(this);if(!(e.parents(r).length>0||"embed"===this.tagName.toLowerCase()&&e.parent("object").length||e.parent(".fluid-width-video-wrapper").length)){e.css("height")||e.css("width")||!isNaN(e.attr("height"))&&!isNaN(e.attr("width"))||(e.attr("height",9),e.attr("width",16));var i=("object"===this.tagName.toLowerCase()||e.attr("height")&&!isNaN(parseInt(e.attr("height"),10))?parseInt(e.attr("height"),10):e.height())/(isNaN(parseInt(e.attr("width"),10))?e.width():parseInt(e.attr("width"),10));if(!e.attr("name")){var a="fitvid"+t.fn.fitVids._count;e.attr("name",a),t.fn.fitVids._count++}e.wrap('<div class="fluid-width-video-wrapper"></div>').parent(".fluid-width-video-wrapper").css("padding-top",100*i+"%"),e.removeAttr("height").removeAttr("width")}})})},t.fn.fitVids._count=0}(window.jQuery||window.Zepto);

jQuery(document).ready(function ($) {

  var $toggle = $('.nav-toggle');
  var $menu = $('.nav-menu');

  $toggle.click(function() {
    $(this).toggleClass('is-active');
    $menu.toggleClass('is-active');
  });

  $('.modal-button').click(function() {
    var target = $(this).data('target');
    $('html').addClass('is-clipped');
    $(target).addClass('is-active');
  });

  $('.modal-background, .modal-close').click(function() {
    $('html').removeClass('is-clipped');
    $(this).parent().removeClass('is-active');
  });

  $('.modal-card-head .delete, .modal-card-foot .button').click(function() {
    $('html').removeClass('is-clipped');
    $('#modal-ter').removeClass('is-active');
  });

  $(document).on('keyup',function(e) {
    if (e.keyCode == 27) {
      $('html').removeClass('is-clipped');
      $('.modal').removeClass('is-active');
    }
  });

  var $highlights = $('.highlight');

  $highlights.each(function() {
    var $el = $(this);
    var copy = '<button class="copy">Copy</button>';
    var expand = '<button class="expand">Expand</button>';
    $el.append(copy);

    if ($el.find('pre code').innerHeight() > 600) {
      $el.append(expand);
    }
  });

  var $highlightButtons = $('.highlight .copy, .highlight .expand');

  $highlightButtons.hover(function() {
    $(this).parent().css('box-shadow', '0 0 0 1px #ed6c63');
  }, function() {
    $(this).parent().css('box-shadow', 'none');
  });

  $('.highlight .expand').click(function() {
    $(this).parent().children('pre').css('max-height', 'none');
  });


  function video_loader(jsonVar){


    for (var i in jsonVar){
      $(".columns.is-mobile.is-multiline.is-gapless")
        .append($("<article>")
          .addClass("sl9_video_article column is-6-mobile is-4-tablet is-4-desktop")
          .append($("<firgure>")
            .append($("<a>")
              .append($("<img>")
                .attr("src", "/img/playBTN.svg")
                .addClass("play-img"))
              .addClass("fancybox fancybox.iframe")
              .attr("href", "https://player.vimeo.com/video/" + jsonVar[i].link + "?color=26a69a&title=0&byline=0&portrait=0&autoplay=1")
              .attr("title",jsonVar[i].title)
              .append($("<img>")
                  .addClass("videoThumb")
                  .attr("src",window.location.origin + jsonVar[i].thumb)
                // .attr("alt","Thumbnail of " + jsonVar[i].sub)
              ))))

    }
  }

  if(window.location.pathname == "/weddings/") {
    video_loader(weddingVid);
  }

  $('.video-container').fitVids();


});

/* * * * * Smooth Scrolling to Anchor Points * * * * */

  $(document).ready(function(){
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 1000, function(){

          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });
  });
